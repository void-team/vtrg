
# Void Team (Big) Repo Guidelines

This Repo contains the list of guidelines used to effectively manage larger Git repos with a larger scale.


The Guidelines in this repo MUST BE ADHERED TO if ANYONE plans on contributing to a project of a large scale (Repos with more than 50 files that contain code of any type. EG: Origins, but not RPG-Test.)


# Rules

## Rule 1: Always explain a Commit.

This rule seems simple, but will solve so many issues down the line. Please, DO NOT leave a pre-generated one liner or indescriptive title for a Git commit. Even a Vague description is good enough to infer what code does. If your code does not have comments that explain what it does, A;ways explain a Commit in its title.

## Rule 2: Merge when Nescessary.

This rule can help make the Git Log LEAGUES easier to read. If working on a seperate branch, DO NOT merge the upstream changes unless it's absolutely nescessary. I know that some devs like to merge a lot to have the latest changes, but unless you're testing for compatability, or intend on working on a new feature that was implemented, or any other viable reason, do not merge until it's nescessary. Helps us track down exactly when and where issues are originated from. Also, explain why a merge occured in the Git message. Makes it easier to understand.

## Rule 3: Stage Changes Seperately

This rule can also help make Git Logs easier to read. If you work on multiple features at the same time, stage the changes on each feature seperately. This also makes it easier to pinpoint where issues originate from. If you can't seperately stage changes (EG. They're all in one file,) seperate the changes with the | symbol.


## Rule 4: Be friendly, not Robotic.

Don't be rude when explaining something to someone. It subconsiously makes a project easier to work on, and makes it more cooperative for everyone. Also, don't just be robotic like the mods of Stack Overflow (Shade Intended.) You're human, they're human, we're all human. Let's act like it.

Also, be fun! Don't be afraid to add little jokes to your Commit message, just make sure they don't overshadow the explanation. 

## Rule 5: Have fun.

Don't work on a project if you're tired of working on it. Void Team can wait. 